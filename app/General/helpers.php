<?php

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;

if (!function_exists('isLogin')) {
    function isLogin()
    {
        return auth()->check();
    }
}

if (!function_exists('user')) {
    function user()
    {
        return auth()->user();
    }
}

if (!function_exists('isAdmin')) {
    function isAdmin()
    {
        $roles = auth()->user()->roles_id === 1 ?? false;
        $isKemenkes = checkPermissionUser('users.kemenkes');
        return $roles || $isKemenkes ? true : false;
    }
}
