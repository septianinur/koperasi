@extends("layout.template")

@push('custom-css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('template/') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('template/') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('template/') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Beranda</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection

  @push('custom-scripts')
  <!-- AdminLTE -->
  <script>
    jQuery('a[data-widget=pushmenu]').click(function(){
      jQuery('.sidebar-mini').toggleClass('sidebar-collapse');
    });
  </script>
  <!-- <script src="{{ asset('template/') }}/dist/js/demo.js"></script> -->
  <!-- AdminLTE dashboard -->
  <!-- <script src="{{ asset('template/') }}/dist/js/pages/dashboard3.js"></script> -->
  <!-- OPTIONAL SCRIPTS -->
  <script src="{{ asset('template/') }}/plugins/chart.js/Chart.min.js"></script> 
  @include('admin.script')
  @endpush