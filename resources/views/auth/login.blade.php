@extends("layout.template")

@section('content')
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="/" class="h1"><b>{{ env('APP_NAME') }}</b></a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Masuk untuk memulai sesi Anda</p>

      <form method="POST" action="{{ route('login') }}">
        <input type="hidden" name="csrf_token" value="{{ csrf_token() }}" />
        @csrf
        <div class="input-group mb-3">
            <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
            </div>
          <input id="email" type="email" placeholder="Alamat email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="off" autofocus>


            <span class="invalid-feedback" role="alert">
                <strong id="error-email"></strong>
            </span>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
            </div>
        <input id="password" type="password" placeholder="Password" class="form-control border-right-0 @error('password') is-invalid @enderror" name="password" required autocomplete="off">
            <!-- <div class="input-group-append bg-white">
                <div class="btn border border-left-0" onclick="show()" id="show">
                  <span class="fas fa-eye"></span>
                </div>
                <div class="btn border border-left-0" onclick="hide()" id="hide" hidden>
                  <span class="fas fa-eye-slash"></span>
                </div>
            </div> -->
          @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
          @enderror
        </div>
        <div class="row">
          <div class="col-8">
            <!-- <div class="icheck-primary">
                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
              <label for="remember">
                Ingat saya
              </label>
            </div> -->
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">{{ __('Masuk') }}</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->
@endsection

@push('custom-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9/sha256.js"></script>
    <script>
        $(document).ready(function(){
            
        function show() {
            $('#show').attr('hidden','true');
            $('#hide').removeAttr('hidden');
            $('#password').attr('type','text');
        }
        function hide(params) {
            $('#show').removeAttr('hidden');
            $('#hide').attr('hidden','true');
            $('#password').attr('type','password');
        }
        });
        
    </script>
@endpush

