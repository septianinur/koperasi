<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::post('login', '\App\Http\Controllers\Auth\LoginController@authenticate');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('/');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'users', 'middleware' => ['auth'], 'as' => 'users.'], function () {
    Route::get('/', [App\Http\Controllers\UserController::class, 'index'])->name('index');
    Route::get('datatable', [App\Http\Controllers\UserController::class, 'datatable'])->name('datatable');
    Route::get('add', [App\Http\Controllers\UserController::class, 'create'])->name('add');
    Route::post('add/store', [App\Http\Controllers\UserController::class, 'store'])->name('add.store');
    Route::get('edit/{id}', [App\Http\Controllers\UserController::class, 'edit'])->name('edit');
    Route::post('update/{id}', [App\Http\Controllers\UserController::class, 'update'])->name('edit.update');
    Route::delete('delete/{id}', [App\Http\Controllers\UserController::class, 'destroy'])->name('delete');
});