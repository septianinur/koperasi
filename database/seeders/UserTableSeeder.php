<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		\DB::table('users')->insert([
			'name' => env('DEFAULT_NAME', 'Admin'),
			'email' => env('DEFAULT_EMAIL', 'admin@admin.com'),
			'password' => \Hash::make(env('DEFAULT_PASSWORD', 12345678)),
		]);
	}
}
