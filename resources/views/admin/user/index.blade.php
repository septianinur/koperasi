@extends("layout.template")

@push('custom-css')
<!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('template/') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('template/') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('template/') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <style>
        div.dataTables_wrapper div.dataTables_filter input {
            padding-right: 1%
        }
    </style>
@endpush

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                <h1>Data Pengguna</h1>
                </div>
            </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            @if (user()->roles_id != 2)
                                <div class="card-header">
                                    <div class="row">
                                        <a href="{{ route('users.add') }}" class="btn btn-success" id="btn-upload">
                                            <i class="fa fa-plus"></i> Buat Akun
                                        </a>
                                    </div>
                                </div>
                            @endif
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="user-table" class="table table-bordered table-striped" style="width: 100%">
                                <thead>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Alamat Email</th>
                                    <th>Tanggal dibuat</th>
                                    <th>Aksi</th>
                                </thead>
                                <tbody>
                                </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            </section>


    </div>

@endsection

@push('custom-scripts')
@include("admin.user.script")
@endpush
