<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if (@$this->id) {
            return [
                'name' => 'required|string',
                'email' => ['required', 'string', Rule::unique('users')->ignore($this->id), 'email',],
                'password' => ['confirmed', Password::min(8), 'nullable'],
            ];
        } else {
            return [
                'name' => 'required|string',
                'email' => ['required', 'string', Rule::unique('users')->ignore($this->id), 'email',],
                'password' => ['required', 'confirmed', 'min:8'],
            ];
        }
    }
    public function messages()
    {
        return [
            'name.required' => 'Nama harus di isi',
            'email.email' => 'Email harus di isi dengan tipe email',
            'email.unique' => 'Email sudah digunakan',
            'email.required' => 'Email harus di isi',
            'password.required' => 'Password harus di isi',
            'password.min' => 'Password minimal 8 karakter',
            'password.dumbpwd' => ''
        ];
    }

}
