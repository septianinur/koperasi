@extends("layout.template")

@push('custom-css')
<style>
    .swal-modal{
        width: 375px !important;
    }
    .swal-footer{
        text-align: center !important;
    }
</style>
@endpush

@section('content')

@if (old())
  {{-- expr --}}
  {{-- @dd(old()) --}}
@endif
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                <h1>{{ (@$data) ? 'Ubah' : 'Buat' }} Pengguna</h1>
                </div>
            </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                          <form action="{{ (@$data) ? route('users.edit.update', @$data->id) : route('users.add.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <!-- /.card-header -->
                            <div class="card-body">
                              <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Nama <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <input type="text" name="name" class="form-control" id="name" value="{{ (@$data->name) ?? old('name') }}" placeholder="Nama" required>
                                  {!! @$errors ? $errors->first('name', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-nama"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="email" class="col-sm-2 col-form-label">Alamat Email <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <input type="email" name="email" class="form-control" value="{{ (@$data->email) ?? old('email') }}" id="email" placeholder="Alamat Email" required>
                                  {!! @$errors ? $errors->first('email', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-email"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="password" class="col-sm-2 col-form-label">Password {!! (@$data) ? '' : '<span class="required-field-sign"></span>' !!}</label>
                                <div class="col-sm-10">
                                      <div class="input-group">
                                          <input type="password" name="password" class="form-control border-right-0" id="password" placeholder="Password" {{ @$data ? '' : 'required' }}>
                                          <!-- <div class="input-group-append bg-white">
                                              <div class="btn border border-left-0" onclick="show_password()" id="show">
                                                <span class="fas fa-eye"></span>
                                              </div>
                                              <div class="btn border border-left-0" onclick="hide_password()" id="hide" hidden>
                                                <span class="fas fa-eye-slash"></span>
                                              </div>
                                          </div> -->
                                      </div>
                                  {!! @$errors ? $errors->first('password', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-password"></strong>
                                    </span>
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="password_confirmation" class="col-sm-2 col-form-label">Konfirm Password {!! (@$data) ? '' : '<span class="required-field-sign"></span>' !!}</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <input type="password" name="password_confirmation" class="form-control border-right-0" id="password_confirmation" placeholder="Konfirm Password"  {{ @$data ? '' : 'required' }}>
                                        <!-- <div class="input-group-append bg-white">
                                            <div class="btn border border-left-0" onclick="show_password_confirmation()" id="show_form">
                                              <span class="fas fa-eye"></span>
                                            </div>
                                            <div class="btn border border-left-0" onclick="hide_password_confirmation()" id="hide_form" hidden>
                                              <span class="fas fa-eye-slash"></span>
                                            </div>
                                        </div> -->
                                    </div>
                                  {!! @$errors ? $errors->first('password_confirmation', '<code><small>:message</small></code>') : '' !!}
                                </div>
                              </div>
                            </div>

                            <div class="card-footer">
                              <a href="{{ route('users.index') }}" id="batal" class="btn btn-default">Batal</a>
                              <button type="submit" class="btn btn-primary">{{ (@$data) ? 'Ubah' : 'Simpan' }}</button>
                            </div>
                            <!-- /.card-body -->
                          </form>
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            </section>


    </div>

@endsection

@push('custom-scripts')
<script>
  $(document).ready(function(){
    function show() {
      $('#show').attr('hidden','true');
      $('#hide').removeAttr('hidden');
      $('#password').attr('type','text');
    }
    function hide() {
      $('#show').removeAttr('hidden');
      $('#hide').attr('hidden','true');
      $('#password').attr('type','password');
    }
    function show_password_confirmation() {
      $('#show_form').attr('hidden','true');
      $('#hide_form').removeAttr('hidden');
      $('#password_confirmation').attr('type','text');
    }
    function hide_password_confirmation() {
      $('#show_form').removeAttr('hidden');
      $('#hide_form').attr('hidden','true');
      $('#password_confirmation').attr('type','password');
    }
  });
</script>
@include("admin.user.script")
@endpush
