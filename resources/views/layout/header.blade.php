<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('template/') }}/plugins/fontawesome-free/css/all.min.css">

    @stack('custom-css')

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('template/') }}/dist/css/adminlte.min.css">


    @toastr_css
    <style>
        .nav-sidebar .menu-is-opening>.nav-link i.right, .nav-sidebar .menu-open>.nav-link i.right{
            transform: rotate(-180deg);
        }
        .swal-modal{
            width: 375px !important;
        }
        .swal-footer{
            text-align: center !important;
        }
        .head-icon{
            size: 10px;
            padding:5px;
        }
    </style>
</head>
<body class="hold-transition sidebar-mini {{ (request()->is('login')) ? 'login-page' : '' }}">

<!-- Site wrapper -->
<div class="{{ (request()->is('login')) ? '' : 'wrapper' }}">
