<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Models\User;
use DB;
use Auth;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index');
    }

    public function datatable()
    {
        $data = User::all();

        return \DataTables::of($data)
            ->addIndexColumn()
            ->removeColumn('id')
            ->editColumn('created_at', function ($data){
                return Carbon::parse($data->created_at)->timezone('Asia/Jakarta')->format('d-M-Y H:i:s');
            })
            ->addColumn('action', function ($data) {
                
                $action = '<a href="' . route('users.edit', $data->id) . '" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="' . __('Edit user') . '"><i class="fa fa-edit"></i></a>&nbsp;';
                
                if($data->id != 1){
                    $action .= '<form method="post" action="' . route('users.delete', $data->id) . '" style="display: inline">
                    <input type="hidden" name="_token" value="' . csrf_token() . '"><input type="hidden" name="_method" value="delete">
                    <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus pengguna ini?" onclick="return confirm(\'' . __('Hapus pengguna ini?') . '\')"><i class="fa fa-trash"></i></button>&nbsp;
                    </form>';
                }

                return '<div class="d-flex">' . $action . '</div>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $redirect = back();
        try {
            DB::beginTransaction();
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => \Hash::make($request->password),
            ];

            $user = User::create($data);
            $redirect = redirect()->route('users.index');
            toastr()->success('Pengguna baru telah berhasil ditambahkan');
            DB::commit();
        } catch (\Exception $e) {
            \Log::info('Data pengguna baru gagal dibuat detail: ' . substr($e->getMessage(), 0, 1000) . ' at ' . $e->getFile() . ' in line ' . $e->getLine());
            toastr()->error('Gagal menambahkan pengguna !');
            DB::rollback();
        }
        return $redirect;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $check_user = User::find($id);
        if($check_user){
            return view('admin.user.create', ['data' => $check_user]);
        }else{
            toastr()->error('Pengguna tidak ditemukan');
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        try {

            $check_user = User::find($id);

            if ($check_user) {
                DB::beginTransaction();

                if ($request->password) {
                    $password = \Hash::make($request->password);
                } else {
                    $password = $check_user->password;
                }
                $check_user->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => $password,
                ]);
                toastr()->success('Pengguna telah diperbarui');
                DB::commit();
            } else {
                toastr()->error('Pengguna tidak ditemukan');
                DB::rollback();
            }
            return redirect()->route('users.index');
        } catch (\Exception $e) {
            \Log::info('Gagal meperbaharui data pengguna : ' . substr($e->getMessage(), 0, 1000) . ' at ' . $e->getFile() . ' in line ' . $e->getLine());
            toastr()->error('Gagal ubah pengguna !');
            DB::rollback();
        }
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            if ($check = User::find($id)) {
                if($check->id == 1){
                    toastr()->error('Pengguna tidak diizinkan untuk dihapus');
                    DB::rollback();
                }else{
                    $check->delete();
                    toastr()->success('Pengguna telah dihapus');
                    DB::commit();
                }
            } else {
                toastr()->error('Pengguna tidak ditemukan');
                DB::rollback();
            }
        } catch (\Exception $e) {
            \Log::info('Gagal menghapus data pengguna : ' . substr($e->getMessage(), 0, 1000) . ' at ' . $e->getFile() . ' in line ' . $e->getLine());
            toastr()->error('Gagal menghapus pengguna !');
            DB::rollback();
        }
        return back();
    }
}
